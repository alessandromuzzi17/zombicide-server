using System;
using System.Net;
using System.Net.Sockets;

namespace ZombicideServer.Models
{
    public class Player : Pawn
    {
        public TcpClient TcpClient { get; }
        public Socket TcpServer { get; private set; }
        public string Name { get; }
        
        public Player(string id, TcpClient tcpClient, string name) : base(id)
        {
            TcpClient = tcpClient;
            Name = name;

            TryToConnectToServer();
        }

        private void TryToConnectToServer()
        {
            var remoteEndPoint = new IPEndPoint(((IPEndPoint)TcpClient.Client.RemoteEndPoint).Address, 11001);
            var remoteIpAddr = remoteEndPoint.Address;
            TcpServer = new Socket(remoteIpAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            // Connect the socket to the remote endpoint. Catch any errors.  
            try
            {
                TcpClient.Connect(remoteEndPoint);

                Console.WriteLine("Socket connected to {0}", TcpServer.RemoteEndPoint);

                #region Old Example Code

                // // Data buffer for incoming data.  
                // byte[] bytes = new byte[1024];
                // ...
                // // Encode the data string into a byte array.  
                // byte[] msg = Encoding.ASCII.GetBytes("{\"operation\": \"new_user\", \"name\": \"Pippo\"}");
                //
                // // Send the data through the socket.  
                // int bytesSent = sender.Send(msg);
                //
                // // Receive the response from the remote device.  
                // int bytesRec = sender.Receive(bytes);
                // Console.WriteLine("Echoed test = {0}",
                //     Encoding.ASCII.GetString(bytes, 0, bytesRec));

                #endregion
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }
        }
    }
}