namespace ZombicideServer.Models
{
    public class Zombie : Pawn
    {
        private AppMain.ZombieType Type { get; }
        
        public Zombie(string id, AppMain.ZombieType type, float coordX, float coordZ) : base(id, coordX, coordZ)
        {
            Type = type;
        }
    }
}