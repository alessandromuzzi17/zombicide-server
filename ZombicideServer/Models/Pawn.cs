namespace ZombicideServer.Models
{
    public class Pawn
    {
        public string Id { get; }
        public float CoordX { get; set; }
        public float CoordZ { get; set; }
        public float RotationY { get; set; }

        public Pawn(string id)
        {
            Id = id;
        }

        public Pawn(string id, float coordX, float coordZ) : this(id, coordX, coordZ, 0f)
        {
            
        }
        
        public Pawn(string id, float coordX, float coordZ, float rotationY)
        {
            Id = id;
            CoordX = coordX;
            CoordZ = coordZ;
            RotationY = rotationY;
        }
    }
}