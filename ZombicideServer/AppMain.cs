﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ZombicideServer.Models;
using ZombicideServer.Utils;

namespace ZombicideServer
{
    public class AppMain
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public enum ZombieType
        {
            Abominio,
            Deambulante,
            Grassone,
            Corridore,
            Strisciante
        }
        
        #region Constants

        private const int WOUND_CARD_CODE = 28;
        
        #endregion

        #region Public Fields

        public static AppMain instance;
        public static IdGenerator IdGenerator;
        public bool isDebug;
        public Stack<int> spawnDeck { get; private set; }
        public Stack<int> equipDeck { get; private set; }
        public Stack<int> woundDeck { get; private set; }
        public Stack<KeyValuePair<string, int>> usedSpawnCards { get; private set; }
        public Stack<KeyValuePair<string, int>> usedEquipCards { get; private set; }
        // <typo zombie, conteggio rimanenti>
        public Dictionary<ZombieType, int> availableZombies { get; }
        // <id zombie, tipo zombie>
        public Dictionary<string, ZombieType> usedZombies { get; } = new Dictionary<string, ZombieType>();

        #region Game Related Data

        public int gameRows;
        public int gameCols;
        public string[] tiles;
        public Dictionary<string, Player> connectedPlayers = new Dictionary<string, Player>();
        private Dictionary<string, Pawn> pawns = new Dictionary<string, Pawn>();

        #endregion
        
        #endregion

        #region Private Fields

        private ZombicideServer zombicideServer;

        #endregion

        #region Constructor

        public AppMain(string[] args)
        {
            instance = this;
            isDebug = (args.Length > 0) && args[0] == "--debug";
            IdGenerator = new IdGenerator();
            
            # region Add Pre-instantiated objects
            
            pawns.Add("PinkDoor", new Pawn("PinkDoor", 27.09f, -15.23f));
            pawns.Add("BlueDoor", new Pawn("BlueDoor", 23.03f, -15.87f));
            pawns.Add("Exit", new Pawn("Exit", 25.03f, -10.43f));
            pawns.Add("RedCross", new Pawn("RedCross", 25.03f, -6.13f));
            pawns.Add("BlueCross", new Pawn("BlueCross", 23.84f, -2.74f));
            pawns.Add("PinkCross", new Pawn("PinkCross", 27.58f, -3.09f));
            pawns.Add("RedCross (1)", new Pawn("RedCross (1)", -22.4f, -6.13f));
            pawns.Add("RedCross (2)", new Pawn("RedCross (2)", -26.43f, -9.3f));
            pawns.Add("RedCross (3)", new Pawn("RedCross (3)", -26.23f, -1.56f));
            pawns.Add("RedCross (4)", new Pawn("RedCross (4)", -23.32f, -12.43f));
            pawns.Add("RedCross (5)", new Pawn("RedCross (5)", -27.31f, -5.64f));
            pawns.Add("RedCross (6)", new Pawn("RedCross (6)", -23.42f, -0.82f));
            pawns.Add("Door", new Pawn("Door", -28.16f, 2.5f));
            pawns.Add("Door (1)", new Pawn("Door (1)", -23.14f, -16.84f));
            pawns.Add("Door (2)", new Pawn("Door (2)", -27.69f, -15.62f));
            pawns.Add("Door (3)", new Pawn("Door (3)", -27.68f, -20.21f));
            pawns.Add("Door (4)", new Pawn("Door (4)", -23.04f, -1.59f));
            pawns.Add("Door (5)", new Pawn("Door (5)", -27f, -24.59f));
            pawns.Add("Spawn", new Pawn("Spawn", 23.03f, -23.26f));
            pawns.Add("BlueSpawn", new Pawn("BlueSpawn", 27.59f, -19.08f));
            pawns.Add("PinkSpawn", new Pawn("PinkSpawn", 21.99f, -19.68f));
            pawns.Add("Spawn (1)", new Pawn("Spawn (1)", -19.88f, -24.41f));
            pawns.Add("Spawn (2)", new Pawn("Spawn (2)", -22.17f, -28.13f));
            pawns.Add("Spawn (3)", new Pawn("Spawn (3)", -23.21f, -21.04f));
            
            #endregion
            
            Console.Write("Size of the map (rows cols): ");
            var rowCols = Console.ReadLine().Split();
            gameRows = Int32.Parse(rowCols[0]);
            gameCols = Int32.Parse(rowCols[1]);
            Console.WriteLine();
            
            Console.Write("Insert the tiles code with orientation sequentially separated by commas (i.e. 4V up,6R down): ");
            tiles = Console.ReadLine().Split(",");

            #region Decks Creation

            MakeSpawnDeck();
            MakeEquipDeck();
            
            availableZombies = new Dictionary<ZombieType, int>
            {
                {ZombieType.Abominio, 1},
                {ZombieType.Deambulante, 40},
                {ZombieType.Grassone, 8},
                {ZombieType.Corridore, 16},
                {ZombieType.Strisciante, 15}
            };

            #endregion
            
            zombicideServer = new ZombicideServer(this, ProcessReceivedData);

            // Add a handler for a Ctrl-C press
            Console.CancelKeyPress += InterruptHandler;

            // run the chat server
            zombicideServer.Run();
        }

        #endregion

        #region Make Deck Methods
    
        public void MakeSpawnDeck()
        {
            Random rnd = new Random();
            spawnDeck = new Stack<int>(Enumerable.Range(0, 47).OrderBy(x => rnd.Next()));    
            usedSpawnCards = new Stack<KeyValuePair<string, int>>();
        }
        
        public void MakeEquipDeck()
        {
            Random rnd = new Random();
            var tmpStack = new Stack<int>();
            for (int i = 0; i < 6; i++) { tmpStack.Push(0); }
            for (int i = 0; i < 2; i++) { tmpStack.Push(1); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(2); }
            for (int i = 0; i < 6; i++) { tmpStack.Push(3); }
            for (int i = 0; i < 2; i++) { tmpStack.Push(4); }
            for (int i = 0; i < 0; i++) { tmpStack.Push(5); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(6); }
            for (int i = 0; i < 2; i++) { tmpStack.Push(7); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(8); }
            for (int i = 0; i < 3; i++) { tmpStack.Push(9); }
            for (int i = 0; i < 3; i++) { tmpStack.Push(10); }
            for (int i = 0; i < 1; i++) { tmpStack.Push(11); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(12); }
            for (int i = 0; i < 1; i++) { tmpStack.Push(13); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(14); }
            for (int i = 0; i < 3; i++) { tmpStack.Push(15); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(16); }
            for (int i = 0; i < 1; i++) { tmpStack.Push(17); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(18); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(19); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(20); }
            for (int i = 0; i < 2; i++) { tmpStack.Push(21); }
            for (int i = 0; i < 3; i++) { tmpStack.Push(22); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(23); }
            for (int i = 0; i < 2; i++) { tmpStack.Push(24); }
            for (int i = 0; i < 2; i++) { tmpStack.Push(25); }
            for (int i = 0; i < 3; i++) { tmpStack.Push(26); }
            for (int i = 0; i < 4; i++) { tmpStack.Push(27); }
            equipDeck = new Stack<int>(tmpStack.OrderBy(x => rnd.Next()));
            usedEquipCards = new Stack<KeyValuePair<string, int>>();
        }
        
        public JToken GetNewWoundCard()
        {
            return WOUND_CARD_CODE;
        }

        #endregion

        #region Interrupt Handler

        protected void InterruptHandler(object sender, ConsoleCancelEventArgs args)
        {
            zombicideServer.Shutdown();
            args.Cancel = true;
        }

        #endregion
        
        #region Process Received Data Methods

        private void ProcessReceivedData(TcpClient tcpClient, string jsonContent)
        {
            var jsonObject = JObject.Parse(jsonContent);
            switch ((string) jsonObject["operation"])
            {
                case "new_user":
                    PerformNewUser(tcpClient, jsonObject);
                    break;
                
                case "get_table":
                    PerformGetTable(tcpClient, jsonObject);
                    break;
                
                case "move_object":
                    PerformMoveObject(tcpClient, jsonObject, jsonContent);
                    break;
                
                case "rotate_object":
                    PerformRotateObject(tcpClient, jsonObject, jsonContent);
                    break;
                
                case "flip_object":
                    PerformFlipObject(tcpClient, jsonObject, jsonContent);
                    break;
                
                case "release_object":
                    PerformReleaseObject(tcpClient, jsonObject, jsonContent);
                    break;
                
                case "draw_spawn":
                    PerformDrawSpawn(tcpClient);
                    break;
                
                case "undo_draw_spawn":
                    PerformUndoDrawSpawn(tcpClient, jsonObject);
                    break;
                
                case "draw_equip":
                    PerformDrawEquip(tcpClient);
                    break;
                
                case "undo_draw_equip":
                    PerformUndoDrawEquip(tcpClient, jsonObject);
                    break;
                
                case "draw_wound":
                    PerformDrawWound(tcpClient);
                    break;
                
                case "get_zombie":
                    PerformGetZombie(tcpClient, jsonObject, jsonContent);
                    break;
            }
        }
        
        private void PerformNewUser(TcpClient tcpClient, JObject jsonObject)
        {
            var responseJObj = new JObject();
            var playerName = (string) jsonObject["player_name"];
            if (!connectedPlayers.ContainsKey(playerName))
            {
                // changing the port of the remote handler
                var newPlayer = new Player(IdGenerator.GetNewIDString(), tcpClient, playerName);
                newPlayer.TcpServer.Send(Encoding.ASCII.GetBytes("{\"new_msg\":\"Hello World\"}"));
                // adding the new player
                connectedPlayers.Add(playerName, newPlayer);
                pawns.Add(newPlayer.Id, newPlayer);
                responseJObj["result"] = "success";
            }
            else
            {
                responseJObj["result"] = "failed";
            }
                    
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }

        private void PerformGetTable(TcpClient tcpClient, JObject jsonObject)
        {
            var responseJObj = new JObject
            {
                ["rows"] = gameRows, ["cols"] = gameCols, ["tiles"] = new JArray(tiles)
            };
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }

        #region Objects Management Methods

        private void PerformMoveObject(TcpClient tcpClient, JObject jsonObject, string jsonContent)
        {
            var responseJObj = new JObject();
            var pawnId = (string)jsonObject["uid"];
            if (pawns.ContainsKey(pawnId))
            {
                var pawn = pawns[pawnId];
                // if (pawn != null && Math.Abs(pawn.CoordX - (float) jsonObject["from_coord_x"]) < 0.01 && Math.Abs(pawn.CoordZ - (float) jsonObject["from_coord_z"]) < 0.01)
                // {
                if (pawn != null)
                {
                    pawn.CoordX = (float) jsonObject["to_coord_x"];
                    pawn.CoordZ = (float) jsonObject["to_coord_z"];
                    SendToEachPlayer(jsonContent, tcpClient);
                    responseJObj["result"] = "success";
                }
                else
                {
                    responseJObj["result"] = "failed";
                }
            }
            else
            {
                responseJObj["result"] = "failed";
            }
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }
        
        private void PerformRotateObject(TcpClient tcpClient, JObject jsonObject, string jsonContent)
        {
            var responseJObj = new JObject();
            var pawnId = (string)jsonObject["uid"];
            if (pawns.ContainsKey(pawnId))
            {
                var pawn = pawns[pawnId];
                // if (pawn != null && Math.Abs(pawn.RotationY - (float) jsonObject["from_rotate_y"]) < 0.01)
                // {
                if (pawn != null)
                {
                    pawn.RotationY = (float) jsonObject["to_rotate_y"];
                    SendToEachPlayer(jsonContent, tcpClient);
                    responseJObj["result"] = "success";
                }
                else
                {
                    responseJObj["result"] = "failed";
                }
            }
            else
            {
                responseJObj["result"] = "failed";
            }
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }
        
        private void PerformFlipObject(TcpClient tcpClient, JObject jsonObject, string jsonContent)
        {
            var responseJObj = new JObject();
            var pawnId = (string)jsonObject["uid"];
            if (pawns.ContainsKey(pawnId))
            {
                var pawn = pawns[pawnId];
                if (pawn != null)
                {
                    SendToEachPlayer(jsonContent, tcpClient);
                    responseJObj["result"] = "success";
                }
                else
                {
                    responseJObj["result"] = "failed";
                }
            }
            else
            {
                responseJObj["result"] = "failed";
            }
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }
        
        /**
            Richiesta
            {
	            "operation": "object_released"
	            "uid": "254"
            }
            Risposta
            Caso positivo:
            {
	            "result": "success"
            }
            Caso negativo:
            {
	            "result": "failed"
            }
         */
        private void PerformReleaseObject(TcpClient tcpClient, JObject jsonObject, string jsonContent)
        {
            var responseJObj = new JObject();
            var pawnId = (string)jsonObject["uid"];
            if (pawns.ContainsKey(pawnId))
            {
                pawns.Remove(pawnId);
                if (usedZombies.ContainsKey(pawnId))
                {
                    // increase counter for released zombie type
                    availableZombies[usedZombies[pawnId]]++;
                    // remove the zombie from the used ones
                    usedZombies.Remove(pawnId);
                }
                SendToEachPlayer(jsonContent, tcpClient);
                responseJObj["result"] = "success";
            }
            else
            {
                responseJObj["result"] = "failed";
            }
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }

        #endregion
        
        #region Cards Management Methods

        /**
            Richiesta
            {
	            "operation": "draw_spawn"
            }
            Risposta
            {
                "result": "success",
	            "uid": "254",
                "card_id": "15"
            }
         */
        private void PerformDrawSpawn(TcpClient tcpClient)
        {
            var responseJObj = new JObject();

            if (spawnDeck.Count == 0)
            {
                MakeSpawnDeck();
            }
            
            var newCard = spawnDeck.Pop();
            var newCardUid = IdGenerator.GetNewIDString();
            usedSpawnCards.Push(new KeyValuePair<string, int>(newCardUid, newCard));
            responseJObj["result"] = "success";
            responseJObj["uid"] = newCardUid;
            responseJObj["card_id"] = newCard;
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }

        /**
            Richiesta
            {
	            "operation": "undo_draw_spawn"
            }
            Risposta
            Caso positivo:
            {
                "result": "success"
            }
            Caso Negativo:
            {
                "result": "failed",
                "reason": "La tua carta non è l'ultima ad essere stata pescata"
            }
         */
        private void PerformUndoDrawSpawn(TcpClient tcpClient, JObject jObject)
        {
            var responseJObj = new JObject();

            if (((string) jObject["uid"]) == usedSpawnCards.Peek().Key)
            {
                spawnDeck.Push(usedSpawnCards.Pop().Value);
                responseJObj["result"] = "success";
            }
            else
            {
                responseJObj["result"] = "failed";
                responseJObj["reason"] = "La tua carta non è l'ultima ad essere stata pescata";
            }

            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }
        
        /**
            Richiesta
            {
	            "operation": "draw_equip"
            }
            Risposta
            {
                "result": "success",
	            "uid": "254",
                "card_id": "15"
            }
         */
        private void PerformDrawEquip(TcpClient tcpClient)
        {
            var responseJObj = new JObject();

            if (equipDeck.Count == 0)
            {
                MakeEquipDeck();
            }
            
            var newCard = equipDeck.Pop();
            var newCardUid = IdGenerator.GetNewIDString();
            usedEquipCards.Push(new KeyValuePair<string, int>(newCardUid, newCard));
            responseJObj["result"] = "success";
            responseJObj["uid"] = newCardUid;
            responseJObj["card_id"] = newCard;
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }
        
        /**
            Richiesta
            {
	            "operation": "undo_draw_equip"
            }
            Risposta
            Caso positivo:
            {
                "result": "success"
            }
            Caso Negativo:
            {
                "result": "failed",
                "reason": "La tua carta non è l'ultima ad essere stata pescata"
            }
         */
        private void PerformUndoDrawEquip(TcpClient tcpClient, JObject jObject)
        {
            var responseJObj = new JObject();

            if ((string) jObject["uid"] == usedEquipCards.Peek().Key)
            {
                equipDeck.Push(usedEquipCards.Pop().Value);
                responseJObj["result"] = "success";
            }
            else
            {
                responseJObj["result"] = "failed";
                responseJObj["reason"] = "La tua carta non è l'ultima ad essere stata pescata";
            }

            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }
        
        /**
            Richiesta
            {
	            "operation": "draw_wound"
            }
            Risposta
            {
                "result": "success",
	            "uid": "254",
                "card_id": "15"
            }
         */
        private void PerformDrawWound(TcpClient tcpClient)
        {
            var responseJObj = new JObject();

            var newCard = GetNewWoundCard();
            var newCardUid = IdGenerator.GetNewIDString();
            responseJObj["result"] = "success";
            responseJObj["uid"] = newCardUid;
            responseJObj["card_id"] = newCard;
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }

        #endregion

        #region Zombies Management Methods

        /**
            Richiesta
            {
	            "operation": "get_zombie",
	            "type": "abominio"
            }
            Risposta
            Caso positivo:
            {
                "result": "success",
	            "uid": "254",
	            "x": 12.5,
	            "z": 25.4
            }
            Caso negativo:
            {
            "result": "failed"
            }
         */
        private void PerformGetZombie(TcpClient tcpClient, JObject jsonObject, string jsonContent)
        {
            var responseJObj = new JObject();
            var zombieType = JsonConvert.DeserializeObject<ZombieType>((string)jsonObject["type"], new StringEnumConverter());
            if (availableZombies[zombieType] > 0)
            {
                availableZombies[zombieType]--;
                var newZombieId = IdGenerator.GetNewIDString();
                usedZombies.Add(newZombieId, zombieType);
                responseJObj["result"] = "success";
                responseJObj["uid"] = newZombieId;
                var randomPos = Utils.Utils.GetRandomPosInDefaultSpace();
                pawns.Add(newZombieId, new Zombie(newZombieId, zombieType, randomPos.X, randomPos.Z));
                responseJObj["x"] = randomPos.X;
                responseJObj["z"] = randomPos.Z;
                jsonObject["uid"] = newZombieId;
                jsonObject["x"] = randomPos.X;
                jsonObject["z"] = randomPos.Z;
                SendToEachPlayer(jsonObject.ToString(), tcpClient);
            }
            else
            {
                responseJObj["result"] = "failed";
            }
            
            zombicideServer.Send(tcpClient, responseJObj.ToString());
        }

        #endregion

        #endregion

        #region Send To All Players Method

        private void SendToEachPlayer(string jsonContent, TcpClient exceptPlayerTcpClient = null)
        {
            foreach (var player in connectedPlayers.Values)
            {
                if (player.TcpClient != exceptPlayerTcpClient)
                {
                    byte[] msg = Encoding.ASCII.GetBytes(jsonContent);
        
                    // Send the data through the socket.  
                    player.TcpServer.Send(msg);
                }
            }
        }

        #endregion
    }
}