﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZombicideServer.Utils;

namespace ZombicideServer
{
    class AppStart
    {
        private static void Main(string[] args)
        {
            new AppMain(args);
        }
    }
}