//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Net;
//using System.Net.Sockets;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using Newtonsoft.Json.Linq;
//using ZombicideServer.Models;
//
//
//namespace ZombicideServer
//{
//    // State object for reading client data asynchronously  
//    public class StateObject {  
//        // Client  socket.  
//        public Socket workSocket = null;  
//        // Size of receive buffer.  
//        public const int BufferSize = 1024;  
//        // Receive buffer.  
//        public byte[] buffer = new byte[BufferSize];  
//        // Received data string.  
//        public StringBuilder sb = new StringBuilder();
//    }  
//    
//    public class ZombicideServerUnused
//    {
//        #region Private FIelds
//
//        private AppMain appMain;
//        private int gameRows;
//        private int gameCols;
//        private string[] tiles;
//        private List<Pawn> pawns = new List<Pawn>();
//
//        private List<Player> connectedPlayers = new List<Player>();
//        
//        #endregion
//
//        public ZombicideServerUnused()
//        {
//            Console.Write("Size of the map (rows cols): ");
//            var rowCols = Console.ReadLine().Split();
//            gameRows = Int32.Parse(rowCols[0]);
//            gameCols = Int32.Parse(rowCols[1]);
//            Console.WriteLine();
//            
//            Console.Write("Insert the tiles code with orientation sequentially separated by commas (i.e. 4V up,6R down): ");
//            tiles = Console.ReadLine().Split(",");
//            
//            Console.WriteLine("Server started. Game started.");
//            StartListening();
//        }
//
//        // Thread signal.  
//        public ManualResetEvent allDone = new ManualResetEvent(false);
//
//        public void StartListening() {  
//            // Establish the local endpoint for the socket.  
//            // The DNS name of the computer  
//            // running the listener is "host.contoso.com".  
//            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());  
//            IPAddress ipAddress = ipHostInfo.AddressList.First(it => it.AddressFamily == AddressFamily.InterNetwork);  
//            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);  
//      
//            // Create a TCP/IP socket.  
//            Socket listener = new Socket(ipAddress.AddressFamily,  
//                SocketType.Stream, ProtocolType.Tcp );  
//      
//            // Bind the socket to the local endpoint and listen for incoming connections.  
//            try {  
//                listener.Bind(localEndPoint);  
//                listener.Listen(100);
//                Console.WriteLine($"Server started on IP: {ipAddress}  Port: 11000");
//
//                while (true) {  
//                    // Set the event to nonsignaled state.  
//                    allDone.Reset();  
//      
//                    // Start an asynchronous socket to listen for connections.  
//                    Console.WriteLine("Waiting for a connection...");  
//                    listener.BeginAccept(
//                        new AsyncCallback(AcceptCallback),  
//                        listener );  
//      
//                    // Wait until a connection is made before continuing.  
//                    allDone.WaitOne();  
//                }  
//      
//            } catch (Exception e) {  
//                Console.WriteLine(e.ToString());  
//            }  
//      
//            Console.WriteLine("\nPress ENTER to continue...");  
//            Console.Read();  
//      
//        }  
//      
//        public void AcceptCallback(IAsyncResult ar) {  
//            // Signal the main thread to continue.  
//            allDone.Set();  
//      
//            // Get the socket that handles the client request.  
//            Socket listener = (Socket) ar.AsyncState;  
//            Socket handler = listener.EndAccept(ar);
//            byte[] buffer = new byte[1024];
//            string content = String.Empty;
//            // Create the state object.  
//            while (handler.Connected)
//            {
//                // StateObject state = new StateObject();  
//                // state.workSocket = handler;
//                var bytesRead = handler.Receive(buffer);
//                // handler.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0,  
//                //     new AsyncCallback(ReadCallback), state);
//                if (bytesRead > 0) {  
//                    // There  might be more data, so store the data received so far.  
//                    content = Encoding.ASCII.GetString(buffer, 0, bytesRead).ToString();  
//
//                    // All the data has been read from the
//                    // client. Display it on the console.  
//                    Console.WriteLine("Read {0} bytes from socket. \n Data : {1}", content.Length, content );
//                    ProcessReceivedData(handler, content);
//                } 
//            }
//        }  
//      
//        public void ReadCallback(IAsyncResult ar) {  
//            String content = String.Empty;  
//      
//            // Retrieve the state object and the handler socket  
//            // from the asynchronous state object.  
//            StateObject state = (StateObject) ar.AsyncState;  
//            Socket handler = state.workSocket;  
//      
//            // Read data from the client socket.
//            int bytesRead = handler.EndReceive(ar);  
//      
//            if (bytesRead > 0) {  
//                // There  might be more data, so store the data received so far.  
//                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));  
//                
//                content = state.sb.ToString();
//                // All the data has been read from the
//                // client. Display it on the console.  
//                Console.WriteLine("Read {0} bytes from socket. \n Data : {1}", content.Length, content );
//                ProcessReceivedData(handler, content);
//            } 
//            
//            // // Repeat Receive
//            // handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,  
//            //     new AsyncCallback(ReadCallback), state); 
//        }
//
//        private void ProcessReceivedData(Socket handler, string jsonContent)
//        {
//            var jsonObject = JObject.Parse(jsonContent);
//            switch ((string) jsonObject["operation"])
//            {
//                case "new_user":
//                    PerformNewUser(handler, jsonObject);
//                    break;
//                
//                case "get_table":
//                    PerformGetTable(handler, jsonObject);
//                    break;
//                
//                case "object_moved":
//                    PerformObjectMoved(handler, jsonObject, jsonContent);
//                    break;
//                
//                case "draw_spawn":
//                    PerformDrawSpawn(handler);
//                    break;
//                
//                case "undo_draw_spawn":
//                    PerformUndoDrawSpawn(handler, jsonObject);
//                    break;
//                
//                case "draw_equip":
//                    PerformDrawEquip(handler);
//                    break;
//                
//                case "undo_draw_equip":
//                    PerformUndoDrawEquip(handler, jsonObject);
//                    break;
//                
//                case "draw_wound":
//                    PerformDrawWound(handler);
//                    break;
//            }
//        }
//
//        private void PerformNewUser(Socket handler, JObject jsonObject)
//        {
//            var responseJObj = new JObject();
//            var playerName = (string) jsonObject["name"];
//            if (pawns.All(it => (it as Player)?.Name != playerName))
//            {
//                // changing the port of the remote handler
//                var newEndPoint = new IPEndPoint(((IPEndPoint)handler.RemoteEndPoint).Address, 11001);
//                var newSocket = new Socket(newEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
//                var newPlayer = new Player(AppMain.IdGenerator.getNewID(), newSocket, playerName);
//                // adding the new player
//                connectedPlayers.Add(newPlayer);
//                pawns.Add(newPlayer);
//                responseJObj["result"] = "success";
//            }
//            else
//            {
//                responseJObj["result"] = "failed";
//            }
//                    
//            Send(handler, responseJObj.ToString());
//        }
//
//        private void PerformGetTable(Socket handler, JObject jsonObject)
//        {
//            var responseJObj = new JObject
//            {
//                ["rows"] = gameRows, ["cols"] = gameCols, ["tiles"] = new JArray(tiles)
//            };
//            Send(handler, responseJObj.ToString());
//        }
//
//        private void PerformObjectMoved(Socket handler, JObject jsonObject, string jsonContent)
//        {
//            var responseJObj = new JObject();
//            var pawn = pawns.FirstOrDefault(it => it.Id == (int) jsonObject["pawn_id"]);
//            if (pawn != null && pawn.CoordX == (float) jsonObject["from_coord_x"] && pawn.CoordY == (float) jsonObject["from_coord_y"])
//            {
//                pawn.CoordX = (float) jsonObject["to_coord_x"];
//                pawn.CoordY = (float) jsonObject["to_coord_y"];
//                SendToEachPlayer(jsonContent);
//                responseJObj["result"] = "success";
//            }
//            else
//            {
//                responseJObj["result"] = "failed";
//            }
//            
//            Send(handler, responseJObj.ToString());
//        }
//
//        /**
//            Richiesta
//            {
//	            "operation": "draw_spawn"
//            }
//            Risposta
//            {
//                "result": "success",
//	            "uid": "254",
//                "card_id": "15"
//            }
//         */
//        private void PerformDrawSpawn(Socket handler)
//        {
//            var responseJObj = new JObject();
//
//            if (appMain.spawnDeck.Count == 0)
//            {
//                appMain.MakeSpawnDeck();
//            }
//            
//            var newCard = appMain.spawnDeck.Pop();
//            var newCardUid = AppMain.IdGenerator.getNewID();
//            appMain.usedSpawnCards.Push(new KeyValuePair<int, int>(newCardUid, newCard));
//            responseJObj["result"] = "success";
//            responseJObj["uid"] = newCardUid;
//            responseJObj["card_id"] = newCard;
//            
//            Send(handler, responseJObj.ToString());
//        }
//
//        /**
//            Richiesta
//            {
//	            "operation": "undo_draw_spawn"
//            }
//            Risposta
//            Caso positivo:
//            {
//                "result": "success"
//            }
//            Caso Negativo:
//            {
//                "result": "failed",
//                "reason": "La tua carta non è l'ultima ad essere stata pescata"
//            }
//         */
//        private void PerformUndoDrawSpawn(Socket handler, JObject jObject)
//        {
//            var responseJObj = new JObject();
//
//            if (((int) jObject["uid"]) == appMain.usedSpawnCards.Peek().Key)
//            {
//                appMain.spawnDeck.Push(appMain.usedSpawnCards.Pop().Value);
//                responseJObj["result"] = "success";
//            }
//            else
//            {
//                responseJObj["result"] = "failed";
//                responseJObj["reason"] = "La tua carta non è l'ultima ad essere stata pescata";
//            }
//
//            Send(handler, responseJObj.ToString());
//        }
//        
//        /**
//            Richiesta
//            {
//	            "operation": "draw_equip"
//            }
//            Risposta
//            {
//                "result": "success",
//	            "uid": "254",
//                "card_id": "15"
//            }
//         */
//        private void PerformDrawEquip(Socket handler)
//        {
//            var responseJObj = new JObject();
//
//            if (appMain.equipDeck.Count == 0)
//            {
//                appMain.MakeEquipDeck();
//            }
//            
//            var newCard = appMain.equipDeck.Pop();
//            var newCardUid = AppMain.IdGenerator.getNewID();
//            appMain.usedEquipCards.Push(new KeyValuePair<int, int>(newCardUid, newCard));
//            responseJObj["result"] = "success";
//            responseJObj["uid"] = newCardUid;
//            responseJObj["card_id"] = newCard;
//            
//            Send(handler, responseJObj.ToString());
//        }
//        
//        /**
//            Richiesta
//            {
//	            "operation": "undo_draw_equip"
//            }
//            Risposta
//            Caso positivo:
//            {
//                "result": "success"
//            }
//            Caso Negativo:
//            {
//                "result": "failed",
//                "reason": "La tua carta non è l'ultima ad essere stata pescata"
//            }
//         */
//        private void PerformUndoDrawEquip(Socket handler, JObject jObject)
//        {
//            var responseJObj = new JObject();
//
//            if ((int) jObject["uid"] == appMain.usedEquipCards.Peek().Key)
//            {
//                appMain.equipDeck.Push(appMain.usedEquipCards.Pop().Value);
//                responseJObj["result"] = "success";
//            }
//            else
//            {
//                responseJObj["result"] = "failed";
//                responseJObj["reason"] = "La tua carta non è l'ultima ad essere stata pescata";
//            }
//
//            Send(handler, responseJObj.ToString());
//        }
//        
//        /**
//            Richiesta
//            {
//	            "operation": "draw_wound"
//            }
//            Risposta
//            {
//                "result": "success",
//	            "uid": "254",
//                "card_id": "15"
//            }
//         */
//        private void PerformDrawWound(Socket handler)
//        {
//            var responseJObj = new JObject();
//
//            var newCard = appMain.GetNewWoundCard();
//            var newCardUid = AppMain.IdGenerator.getNewID();
//            responseJObj["result"] = "success";
//            responseJObj["uid"] = newCardUid;
//            responseJObj["card_id"] = newCard;
//            
//            Send(handler, responseJObj.ToString());
//        }
//
//        #region Send Methods
//
//        private void Send(Socket handler, String data) {  
//            // Convert the string data to byte data using ASCII encoding.  
//            byte[] byteData = Encoding.ASCII.GetBytes(data);
//
//            // Begin sending the data to the remote device.  
//            handler.BeginSend(byteData, 0, byteData.Length, 0,  
//                new AsyncCallback(SendCallback), handler);  
//        }  
//
//        private void SendToEachPlayer(string jsonContent)
//        {
//            pawns.ForEach(it =>
//            {
//                if (it is Player)
//                {
//                    Send(((Player)it).TcpClient, jsonContent);
//                }
//            });
//        }
//        
//        private void SendCallback(IAsyncResult ar) {  
//            try {  
//                // Retrieve the socket from the state object.  
//                Socket handler = (Socket) ar.AsyncState;  
//      
//                // Complete sending the data to the remote device.  
//                int bytesSent = handler.EndSend(ar);  
//                Console.WriteLine("Sent {0} bytes to client.", bytesSent);  
//      
//                handler.Shutdown(SocketShutdown.Both);  
//                handler.Close();  
//      
//            } catch (Exception e) {  
//                Console.WriteLine(e.ToString());  
//            }  
//        }
//
//        #endregion
//    }
//}