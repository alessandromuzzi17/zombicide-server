using System;

namespace ZombicideServer.Utils
{
    public class ConsoleHelper
    {
        public static void WriteLine(string message)
        {
            if (AppMain.instance.isDebug)
            {
                Console.WriteLine(message);
            }
        }
        
        public static void Write(string message)
        {
            if (AppMain.instance.isDebug)
            {
                Console.Write(message);
            }
        }
    }
}